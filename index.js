const btnRight = document.getElementById('right')
const btnLeft = document.getElementById('left')
const slides = [...document.querySelectorAll('.slider__item')]

let zakladka = 0

btnRight.addEventListener('click', () => {
	slides[zakladka].classList.remove('active')

	zakladka = zakladka + 1
	// zakladka++
	if (zakladka > slides.length - 1) {
		zakladka = 0
	}

	slides[zakladka].classList.add('active')
})

btnLeft.addEventListener('click', () => {
	slides[zakladka].classList.remove('active')

	zakladka = zakladka - 1
	// zakladka--
	if (zakladka < 0) {
		zakladka = slides.length - 1
	}

	slides[zakladka].classList.add('active')
})

setInterval(() => {
	slides[zakladka].classList.remove('active')

	zakladka = zakladka + 1
	// zakladka++
	if (zakladka > slides.length - 1) {
		zakladka = 0
	}

	slides[zakladka].classList.add('active')
}, 1500)
